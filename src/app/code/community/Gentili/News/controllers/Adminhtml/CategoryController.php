<?php
/**
 * Gentili News
 */

/**
 * Class Gentili_News_Adminhtml_CategoryController
 *
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */

class Gentili_News_Adminhtml_CategoryController extends Mage_Adminhtml_Controller_Action
{

    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * newAction
     */
    public function newAction()
    {
        $this->_forward('edit'); //contenuto dell'edit
    }

    /**
     * editAction
     */
    public function editAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * saveAction
     */
    public function saveAction()
    {
        // get category id
        $categoryId = $this->getRequest()->getParam('category_id');
        if($categoryId){
            //load object
            $model = Mage::getModel('gentili_news/category')->load($categoryId);
            //verify if this is a valid object
            if(!$model || !$model->getId()){
                Mage::getSingleton('adminhtml/session')->addError($this->__('There was an error when loading the category. Please, return to the previous page and try again'));
                return $this->_redirect('*/*/index');
            }
        }else{
            $model = Mage::getModel('gentili_news/category');
        }


        //manipulate object
        try{
            $model->setCode($this->getRequest()->getParam('code'));
            $model->setTitle($this->getRequest()->getParam('title'));
            $model->setStatus($this->getRequest()->getParam('status'));

            $model->save();
        }catch (Exception $e){
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($this->__('There was an error when loading the category. Please, return to the previous page and try again'));
            return $this->_redirect('*/*/index');
        }

        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The category successfully saved'));

        if($this->getRequest()->getParam('back') && $this->getRequest()->getParam('back') == "edit"){
            return $this->_redirect('*/*/edit' , array('category_id' => $model->getId()));
        }
        return $this->_redirect('*/*/index');

    }

    /**
     * deleteAction
     */
    public function deleteAction()
    {

    }

    /**
     * _isAllowed
     * @return bool
     */
    public function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('gentili_news');
    }

}