<?php
/**
 * Gentili News
 */

/**
 * Class Gentili_News_Block_Adminhtml_Story
 *
 * Gentili News Admin Story Grid Container Block.
 * Container build
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Adminhtml_StoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * indexAction()
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }


    /**
     * saveAction
     */
    public function saveAction()
    {

    }

    /**
     * editAction
     */
    public function editAction()
    {

    }

    /**
     * deleteAction
     */
    public function deleteAction()
    {

    }

    /**
     * _isAllowed
     * @return bool
     */
    public function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('gentili/news');
    }

}