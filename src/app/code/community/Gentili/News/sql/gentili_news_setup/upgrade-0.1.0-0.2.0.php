<?php
/**
 * Gentili Setup Story Table
 */

/**
 * Gentili News SQL Installer
 * @author Simone Gentili simone.gentili@thinkopen.it
 * @version 0.1.0
 * @package CMS
 * @license GNU version 3
 */
$installer = $this;
$installer->startSetup();

$tableStory = $installer
                    ->getConnection()
                    ->newTable($installer->getTable('gentili_news/story'));

$tableStory->addColumn(
    'story_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    null,
    array('nullable' => false, 'identity' => true ,'primary' => true),
    "Store Id"
)->addColumn(
    'title',
    Varien_Db_Ddl_Table::TYPE_TEXT,
    128,
    array('nullable' => false),
    'Story Title'
)->addColumn(
    'thumbnail_path',
    Varien_Db_Ddl_Table::TYPE_TEXT,
    128,
    array('nullable' => false),
    'Story Thumbnail Image'
)->addColumn(
    'content',
    Varien_Db_Ddl_Table::TYPE_TEXT,
    null,
    array('nullable' => false),
    'Story Content'
)->addColumn(
    'status',
    Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    null,
    array('nullable' => false),
    'Story Status'
)->addColumn(
    'category_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    null,
    array('nullable' => false),
    'News Story Category ID'
)->addColumn(
    'created_at',
    Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    null,
    array('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT),
    'Story Created At'
)->addColumn(
    'updated_at',
    Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    null,
    array('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE),
    'Story Updated At'
)->addForeignKey(
    $installer->getFkName(
        'gentili_news/story',
        'category_id',
        'gentili_news/category',
        'category_id'
    ),
    'category_id',    //nome del campo presente all'interno di questa tabella che funziona come chiave esterna
    $installer->getTable('gentili_news/category'),    //tabella esterna in cui è presente la chiave primaria da collegare
    'category_id'    //chiave primaria a cui legare chiave esterna
)->setComment("News Story Table");

try{
    $installer->getConnection()->createTable($tableStory);
}catch (Exception $e){
    Mage::logException($e);
}

$installer->endSetup();