<?php
/**
 * Gentili News Setup
 */

/**
 * Gentili News SQL Installer.
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.1.0
 * @package CMS
 * @license GNU version 3
 */

/** @var $installer Mage_Core_Model_Resource_Setup*/
$installer = $this;
$installer->startSetup(); //start setup

//prepare table for gentili_news/category
$tableCategory = $installer
                    ->getConnection()
                    ->newTable($installer->getTable('gentili_news/category')); //nome_model/entity

//populate the table
/**
 * parametri in addColumn
 *     nome colonna
 *     tipo di colonna
 *     size
 *     array
 *         nullable = non può essere nullo
 *         identity = auto increment
 *         primary = chiave primaria
 *         default = per i timestamp c'è un valore di default per Magento
 *     commento
 */
$tableCategory->addColumn(
    'category_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    null,
    array('nullable' => false , 'identity' => true, 'primary' => true),
    'Category Id'
)->addColumn(
    'code',
    Varien_Db_Ddl_Table::TYPE_TEXT,
    64,
    array('nullable' => false),
    'Category Code'
)->addColumn(
    'title',
    Varien_Db_Ddl_Table::TYPE_TEXT,
    256,
    array('nullable' => false),
    'Category Title'
)->addColumn(
    'status',
    Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    null,
    array('nullable' => false),
    'Category Status'
)->addColumn(
    'created_at',
    Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    null,
    array('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT),
    'Category Created At'
)->addColumn(
    'updated_at',
    Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    null,
    array('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE),
    'Category Updated At'
)->setComment('News Category Table');

//create table
try{
    $installer->getConnection()->createTable($tableCategory);
}catch (Exception $e){
    Mage::logException($e);
}

$installer->endSetup(); //end setup