<?php
/**
 * Gentili News Story
 */

/**
 * Gentili_News_Model_Story
 *
 * Gentili News Story Model
 * @method string getCode()
 * @method setCode(string $code)
 * @method string getTitle()
 * @method setTitle(string $title)
 * @method string getContent()
 * @method setContent(string $content)
 * @method string getThumbnailPath()
 * @method setThumbnailPath(string $thumbnailPath)
 * @method int getCategoryId()
 * @method boolean getStatus()
 * @method setStatus(boolean $status)
 * @method string getCreatedAt()
 * @method string getUpdatedAt()
 * @method setUpdatedAt(string $updatedAt)
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Model_Story extends Mage_Core_Model_Abstract
{
    /**
     * __constructor
     */
    public function _construct()
    {
        $this->_init('gentili_news/story');
    }

}