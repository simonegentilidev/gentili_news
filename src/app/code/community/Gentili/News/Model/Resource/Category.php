<?php
/**
 * Gentili News Category DB
 */

/**
 * Gentili_News_Model_Resource_Category
 *
 * Gentili News Category Model Resource
 * Connection to category db table
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Model_Resource_Category extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * _construct
     */
    public function _construct()
    {
        $this->_init('gentili_news/category' , 'category_id');
    }

}