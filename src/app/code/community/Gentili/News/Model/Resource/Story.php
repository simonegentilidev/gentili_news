<?php
/**
 * Gentili News Story DB
 */

/**
 * Gentili_News_Model_Resource_Story
 *
 * Gentili News Story Model Resource
 * Connection to story sdb table
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Model_Resource_Story extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('gentili_news/story' , 'story_id');
    }
}