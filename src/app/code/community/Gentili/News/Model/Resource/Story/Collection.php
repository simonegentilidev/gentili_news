<?php
/**
 * Gentili News Story
 */

/**
 * Gentili_News_Model_Resource_Story
 *
 * Gentili News Story Model Resource
 * Connection to category db table
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Model_Resource_Story_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    public function _construct()
    {
        $this->_init('gentili_news/story');
    }
}