<?php
/**
 * Gentili News Helper
 */

/**
 * Class Gentili_News_Helper_Data
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.1.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Model_Categoryobserver
{
    /**
     * createLogEntryl
     *
     * Create a log entry when a category is saved.
     * @param Varien_Event_Observer $observer
     * @return Varien_Event_Observer
     */
    public function createLogEntry(Varien_Event_Observer $observer)
    {
        //get category object
        $category = $observer->getData('category');

        //quality control
        if(!$category || !$category->getId()){
            return $observer;
        }

        //save log
        Mage::log('Category saved ID: ' . $category->getId());
        return $observer;
    }
}