<?php
/**
 * Gentili News
 */

/**
 * Class Gentili_News_Model_Source_Status
 *
 * Gentili News Admin Category Grid Container Block.
 * Container build
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Model_Source_Status
{
    /**
     * toOptionArray
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 0,
                'label' => Mage::helper('gentili_news')->__('Disabled')
            ),
            array(
                'value' => 1,
                'label' => Mage::helper('gentili_news')->__('Enabled')
            )
        );
    }

    /**
     * toGridArray
     * @return array
     */
    public function toGridArray()
    {
        $array = array();
        foreach($this->toOptionArray() as $option) {
            $array[$option['value']] = $option['label'];
        }
        return $array;
    }
}