<?php
/**
 * Gentili News Category Tabs
 */

/**
 * Adminhtml Category Edit Tabs
 * Class Gentili_News_Block_Adminhtml_Category_Grid
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @license GNU version 3
 */
class Gentili_News_Block_Adminhtml_Category_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('gentili_news_category_edit_tabs'); //id di questo blocco qui
        $this->setDestElementId('gentili_news_category_edit_form'); //blocco che verrà utilizzato nel form per default - non è il percorso ma il nome di quel blocco
    }

    /**
     * _beforeToHtml
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    public function _beforeToHtml() //prima di renderizzare
    {
        $this->addTab(
            'category_details',
            array(
                'label' => Mage::helper('gentili_news')->__('Category'),
                'title' => Mage::helper('gentili_news')->__('Category'),
            )
        );
        return parent::_beforeToHtml(); // TODO: Change the autogenerated stub
    }


}