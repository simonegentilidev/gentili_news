<?php
/**
 * Gentili News Story Grid
 */

/**
 * Adminhtml Story Grid
 * Class Gentili_News_Block_Adminhtml_Category_Grid
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @license GNU version 3
 */
class Gentili_News_Block_Adminhtml_Story_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('story_id');
        $this->setDefaultSort('story_id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    public function _prepareCollection()
    {
        $collection = Mage::getModel('gentili_news/story')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn(
            'story_id', //nome colonna
            array(
                'index' => 'story_id', //nome della colonna nel db
                'header' => $this->__('ID'),
                'width' => 50,
                'type' => 'number'
            )
        );

        $this->addColumn(
            'category_id', //nome colonna
            array(
                'index' => 'category_id', //nome della colonna nel db
                'header' => $this->__('ID Categoria'),
                'width' => 50,
                'type' => 'number'
            )
        );

        $this->addColumn(
            'thumbnail_path', //nome colonna
            array(
                'index' => 'thumbnail_path', //nome della colonna nel db
                'header' => $this->__('Thumbnail'),
                'type' => 'text',
            )

        );

        $this->addColumn(
            'title', //nome colonna
            array(
                'index' => 'title', //nome della colonna nel db
                'header' => $this->__('Title'),
                'type' => 'text',
            )

        );

        $this->addColumn(
            'content', //nome colonna
            array(
                'index' => 'content', //nome della colonna nel db
                'header' => $this->__('Content'),
                'type' => 'text',
            )

        );


        $statusValues = Mage::getModel('gentili_news/source_status')->toGridArray();

        $this->addColumn(
            'status', //id colonna per magento (non del db)
            array(
                'index' => 'status', //nome della colonna nel db
                'header' => $this->__('Status'),
                'width' => 100,
                'type' => 'options',
                'options' => $statusValues,
                'renderer' => 'gentili_news/adminhtml_renderer_status' //block in carica di fare il render
            )
        );

        $this->addColumn('action',
            array(
                'header' =>  Mage::helper('gentili_news')->__('Actions'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId', //qual'è il metodo che risulta nell'id della riga
                'actions' => array(
                    array(
                        'caption' => $this->__('Edit'), //label
                        'url' => array('base' => '*/*/edit'), //url in cui faccio la chiamata */*/ -> stesso frontName/controller ma con action -> edit
                        'field' => 'story_id', //parametro che gli passo e il valore lo prende da getter => getId
                    ),
                    array(
                        'caption' => $this->__('Delete'),
                        'url' => array('base' => '*/*/delete'),
                        'field' => 'story_id',
                        'confirm' => $this->__('Are you sure you want to do this?'),
                    )
                ),
                'filter' => false, //possibilità di filtrare
                'sortable' => false, //possibilità di ordinare
                'index' => 'stores',
                'is_system' => true, //cosa di sistema
            )
        );

        return parent::_prepareColumns();

    }

}