<?php
/**
 * Gentili News
 */

/**
 * Class Gentili_News_Block_Adminhtml_Renderer_Status
 *
 * Gentili News Admin Category Grid Container Block.
 * Container build
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Block_Adminhtml_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        if($row->getStatus() == 0){
            return '<span class="grid-severity-minor"><span>' . $this->__('Disabled') . '</span></span>';
        }else if($row->getStatus() == 1){
            return '<span class="grid-severity-notice"><span>' . $this->__('Enabled') . '</span></span>';
        }else{
            return '**NULL**';
        }
    }
}