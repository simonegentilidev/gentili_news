<?php
/**
 * Gentili News Category Tabs
 */

/**
 * Adminhtml Category Edit Tabs
 * Class Gentili_News_Block_Adminhtml_Category_Grid
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @license GNU version 3
 */
class Gentili_News_Block_Adminhtml_Category_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('gentili_news_category_edit_form');
    }

    protected function _prepareForm()
    {
        // prepare form
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form', // id della form
                'method' => 'post', //metodo di passaggio dei dati
                'action' => $this->getUrl('*/*/save', array('category_id' => $this->getRequest()->getParam('category_id'))),
                'enctype' => 'multipart/form-data',
            )
        );

        $form->setData('html_id_prefix', 'category_'); //tutti i campi avranno un id con quel parametro

        //fieldset = gruppo di campi
        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'legend' => $this->__('General Information'), //label
                'class' => 'fieldset-wide', //classecss
            )
        );

        //aggiungiamo i campi nel FIELDSET e non nel form
        $fieldset->addField(
            'code',
            'text',
            array(
                'name' => 'code',
                'label' => $this->__('Code'),
                'title' => $this->__('Code'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'title',
            'text',
            array(
                'name' => 'title',
                'label' => $this->__('Title'),
                'title' => $this->__('Title'),
                'required' => true,
            )
        );

        $statusValues = Mage::getModel('gentili_news/source_status')->toOptionArray();
        $fieldset->addField(
            'status',
            'select',
            array(
                'name' => 'status',
                'label' => $this->__('Status'),
                'title' => $this->__('Status'),
                'required' => true,
                'values' => $statusValues,
            )
        );

        // if category_id is defined, load the object
        $categoryId = $this->getRequest()->getParam('category_id');
        if ($categoryId) {
            $model = Mage::getModel('gentili_news/category')->load($categoryId);
            if (!$model || !$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('There was an error when loading the category. Please, return to the previous page and try again'));
            }
            $form->setValues($model->getData());
        }

        $form->setData('use_container', true); //anche se ci sono altre tab lui lo vedrà come un unico form
        $this->setForm($form);

        return parent::_prepareForm();
    }
}