<?php
/**
 * Gentili News
 */

/**
 * Class Gentili_News_Block_Adminhtml_Story
 *
 * Gentili News Admin Story Grid Container Block.
 * Container build
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Block_Adminhtml_Story extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'gentili_news';
        $this->_controller = 'adminhtml_story';
        $this->_headerText = $this->__('News Story Management');
        parent::__construct();
    }
}