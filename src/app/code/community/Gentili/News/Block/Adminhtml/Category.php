<?php
/**
 * Gentili News
 */

/**
 * Class Gentili_News_Adminhtml_CategoryController
 *
 * Gentili News Admin Category Grid Container Block.
 * Container build
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Gentili_News_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Gentili_News_Block_Adminhtml_Category constructor.
     */
    public function __construct()
    {
        $this->_blockGroup = 'gentili_news'; //module's name
        $this->_controller = 'adminhtml_category'; //path
        $this->_headerText = $this->__('News Categories Menagement');
        parent::__construct(); //calls to grid's layout
    }
}