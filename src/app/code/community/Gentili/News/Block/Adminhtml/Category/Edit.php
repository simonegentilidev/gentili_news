<?php
/**
 *
 */

class Gentili_News_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'category_id'; //identificatore dell'oggetto
        $this->_blockGroup = 'gentili_news'; //modulo
        $this->_controller = 'adminhtml_category'; //path del block
        //pulsante che chiama metodo javascript
        $this->addButton(
            'saveandcontinue',
            array(
                'label' => $this->__('Save and Continue'),
                'onclick' => 'saveAndContinueEdit()',
                'class' => 'save'
            ),
            100
        );

        //della form editForm faccio la submit passando come parametro l'action della form stessa + il parametro back con valore edit
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action + 'back/edit/');
            }
        ";
        parent::__construct();
    }
}