<?php
/**
 * Gentili News Category Grid
 */

/**
 * Adminhtml Category Grid
 * Class Gentili_News_Block_Adminhtml_Category_Grid
 * @author Simone Gentili <simone.gentili@thinkopen.it>
 * @version 0.2.0
 * @license GNU version 3
 */
class Gentili_News_Block_Adminhtml_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('category_id');//grid's id
        $this->setDefaultSort('category_id');//quale colonna da l'ordinamento
        $this->setDefaultDir('asc');//tipo ordinamento
        $this->setSaveParametersInSession(true);//quando faccio la ricerca -> se quella ricerca rimane attiva o no quando vado via e poi ritorno sulla pagina
    }

    //dirò a magento quale oggetto deve essere utilizzato in questa griglia
    //specifico la collection che popolerà la mia griglia
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('gentili_news/category')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        try{

            $this->addColumn(
                    'category_id', //nome colonna
                    array(
                        'index' => 'category_id', //nome della colonna nel db
                        'header' => $this->__('ID'),
                        'width' => 50,
                        'type' => 'number'
                    )
            );

            $this->addColumn(
                'code', //nome colonna
                array(
                    'index' => 'code', //nome della colonna nel db
                    'header' => $this->__('Code'),
                    'width' => 150,
                    'type' => 'text'
                )
            );

            $this->addColumn(
                'title', //nome colonna
                array(
                    'index' => 'title', //nome della colonna nel db
                    'header' => $this->__('Title'),
                    'type' => 'text',
                )

            );

            $statusValues = Mage::getModel('gentili_news/source_status')->toGridArray();

            $this->addColumn(
                'status', //id colonna per magento (non del db)
                array(
                    'index' => 'status', //nome della colonna nel db
                    'header' => $this->__('Status'),
                    'width' => 100,
                    'type' => 'options',
                    'options' => $statusValues,
                    'renderer' => 'gentili_news/adminhtml_renderer_status' //block in carica di fare il render
                )
            );

            $this->addColumn('action',
                array(
                    'header' =>  Mage::helper('gentili_news')->__('Actions'),
                    'width' => '100',
                    'type' => 'action',
                    'getter' => 'getId', //qual'è il metodo che risulta nell'id della riga
                    'actions' => array(
                        array(
                            'caption' => $this->__('Edit'), //label
                            'url' => array('base' => '*/*/edit'), //url in cui faccio la chiamata */*/ -> stesso frontName/controller ma con action -> edit
                            'field' => 'category_id', //parametro che gli passo e il valore lo prende da getter => getId
                        ),
                        array(
                            'caption' => $this->__('Delete'),
                            'url' => array('base' => '*/*/delete'),
                            'field' => 'category_id',
                            'confirm' => $this->__('Are you sure you want to do this?'),
                        )
                    ),
                    'filter' => false, //possibilità di filtrare
                    'sortable' => false, //possibilità di ordinare
                    'index' => 'stores',
                    'is_system' => true, //cosa di sistema
                )
            );

        }catch (Exception $e){
            Mage::logException($e);
        }

        return parent::_prepareColumns();
    }

    /**
     * getRowUrl
     *
     * @param Varien_Object $item
     * @return string
     */
    public function getRowUrl($item)
    {
       return $this->getUrl('*/*/edit', array('category_id' => $item->getId())); //ritorna url di pannello
    }

}